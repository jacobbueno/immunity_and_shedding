% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[
]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdftitle={Immunologic Correlates of Influenza A (H3) Viral Aerosol Shedding from Infected University Students},
  pdfauthor={Authors1; 1Maryland Institute for Applied Environmental Health, College Park, MD 20742; email@umd.edu},
  hidelinks,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[margin=1in]{geometry}
\usepackage{longtable,booktabs}
% Correct order of tables after \paragraph or \subparagraph
\usepackage{etoolbox}
\makeatletter
\patchcmd\longtable{\par}{\if@noskipsec\mbox{}\fi\par}{}{}
\makeatother
% Allow footnotes in longtable head/foot
\IfFileExists{footnotehyper.sty}{\usepackage{footnotehyper}}{\usepackage{footnote}}
\makesavenoteenv{longtable}
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\usepackage{booktabs}
\usepackage{longtable}
\usepackage{array}
\usepackage{multirow}
\usepackage{wrapfig}
\usepackage{float}
\usepackage{colortbl}
\usepackage{pdflscape}
\usepackage{tabu}
\usepackage{threeparttable}
\usepackage{threeparttablex}
\usepackage[normalem]{ulem}
\usepackage{makecell}
\usepackage{xcolor}

\title{Immunologic Correlates of Influenza A (H3) Viral Aerosol Shedding from
Infected University Students}
\author{Authors\textsuperscript{1} \and \textsuperscript{1}Maryland Institute for Applied Environmental Health,
College Park, MD 20742 \and \href{mailto:email@umd.edu}{\nolinkurl{email@umd.edu}}}
\date{}

\begin{document}
\maketitle
\begin{abstract}
The mode of influenza infection is known to influence illness severity
and may be associated with viral shedding into aerosols and upper
respiratory mucosa. Viral replication in the lung and associated aerosol
shedding could be associated with pre-existing antibody levels or
antibody production following infection. In 18 influenza A (H3) cases
from the University of Maryland campus community, we compared H3 viral
shedding into aerosols and onto fomites with pre-existing and
fold-increase in serum antibody to HA and NA antigens. We found no
associations between viral shedding and prexisting antibodies. We found
that those with detectable aerosol shedding had higher N2 antibody
increases following infection compared to those without detectable viral
aerosols. No difference in antibody rise was observed between those with
and without fomite shedding. Our method of population surveillance in a
University setting paried with measures of viral shedding and adaptive
immunity supports characterization of the relationship between influenza
immunogenesis and infectiousness. Compared with others with undetectable
viral aerosols, production of detectable viral aerosols, perhaps driven
in part by lower respiratory tract viral replication, may be linked with
the subsequent generation of a stronger adaptive immune response.
\end{abstract}

\hypertarget{introduction}{%
\subsection{Introduction}\label{introduction}}

\begin{itemize}
\tightlist
\item
  Pre-existing antibodies have been used to predict susceptibility to
  influenza infection and duration of upper respiratory shedding (Hannah
  Maier et al., CID 2020), but little is known about the associations
  between pre-existing antibody levels and viral aerosol shedding among
  those with infection.
\item
  Influenza is an anisotropic disease with inhaled aerosols depositing
  into the lower airways likely to initiate more severe infection than
  upper respiratory exposure. Severe illness has been associated with
  prolonged immune activation (Sook-San Wong et al., JID 2017). Is
  adaptive immune response related to viral with viral aerosol
  production in the lung?
\end{itemize}

\hypertarget{methods}{%
\subsection{Methods}\label{methods}}

\begin{itemize}
\tightlist
\item
  Description of population from university campus cohort, IRB statement
\item
  Viral shedding characterization
\item
  Immunology characterization.

  \begin{itemize}
  \tightlist
  \item
    Blood for immunologic assays was generally taken at infection and
    during convalescent followup at the end of the academic semester.
    When blood was not available at infection, available preinfection
    baseline blood was used for immunologic assays.
  \end{itemize}
\item
  Analysis approach

  \begin{itemize}
  \tightlist
  \item
    Spearman's rank correlation coefficient and Wilcoxon rank-sum and
    Wilcoxon signed-rank tests used because of small sample size.
  \end{itemize}
\end{itemize}

\hypertarget{results}{%
\subsection{Results}\label{results}}

There were 17 influenza A H3 infections with available shedding data,
pre-existing serum antibody data, and convalescent serum antibody data.
There was 1 additional influenza A H3 infection with available shedding
data and pre-existing serum antibody data, without convalescent serum
antibody data. Pre-existing antibody level and fold-increase in antibody
level following infection are plotted for each antigen for H3 infections
(figure 1).

18\% (3/17) H3-infected study participants in this analytical subset had
detectable virus in their exhaled breath aerosols, while 47\% (8/17) had
detectable virus on their phone swab fomite.

Of the 17 participants with baseline and post-infection serum data, 15
had baseline samples at infection and two had baseline samples more than
2 weeks before infection. The mean (sd) number of days between infection
and post-infection serum sampling was 46 (19) with a minimum of 22 days.

No differences were observed in pre-existing H3N2 or H1N1p antibody
levels between H3-infected participants with and without detectable
aerosol or fomite shedding (figure 2). There were no increases in H1
antibodies in those with confirmed H3 infection.

Between baseline and followup sampling, fold increases in N2 ELISA AUC
and N2 NAI titer were greater for those with versus without detectable
aerosol shedding (ELISA p = 0.023, titer p = 0.078). Differences in N2
antibody fold increase were not observed between those with and without
detectable viral shedding onto fomites.

Several strong correlations (spearman rho \textgreater{} 0.75) were
observed between antibody detection methods for H3 and N2 antigens
following H3 infection (figure 3). A statistically significant (at alpha
= 0.05) coefficient of 0.56 was observed between quantitative H3 RNA
copies shed into exhaled breath and increases in N2 titer fold-increase.
A marginally statistically significant (at alpha = 0.10) coefficient of
0.43 was observed between H3 Aerosol virions copies and N2 ELISA
fold-increase.

Although none of the participants with detectable aerosol shedding had a
fever \textgreater37.9 during research clinic observations, they all
reported moderate to severe cough and feelings of sweats/fever/chill,
sore throat, and malaise.

\hypertarget{exploratory-assessment-of-samples-available-for-analysis}{%
\subsubsection{Exploratory assessment of samples available for
analysis}\label{exploratory-assessment-of-samples-available-for-analysis}}

\hypertarget{detectable-aerosol-and-fomite-shedding-and-relationship-with-pre-existing-and-fold-increases-in-ab-levels-pre-existing-serum-samples-for-ab-assessment-taken-from-acute-infection-as-opposed-to-at-baseline-where-available}{%
\subsubsection{Detectable aerosol and fomite shedding and relationship
with pre-existing and fold-increases in Ab levels (pre-existing serum
samples for Ab assessment taken from acute infection, as opposed to at
baseline, where
available)}\label{detectable-aerosol-and-fomite-shedding-and-relationship-with-pre-existing-and-fold-increases-in-ab-levels-pre-existing-serum-samples-for-ab-assessment-taken-from-acute-infection-as-opposed-to-at-baseline-where-available}}

\begin{figure}
\centering
\includegraphics{report_main_text_files/figure-latex/H3 Infection Detectable Viral Shedding in Aerosol and Fomite versus Pre-existing and fold-increase in Ab with pre-existing Ab taken from serum at acute infection if available-1.pdf}
\caption{Figure 2. Pre-existing and fold-increase in antibody levels by
H3 shedding into aerosols and onto fomites during infection}
\end{figure}

\begin{figure}
\centering
\includegraphics{report_main_text_files/figure-latex/Detectable Viral Shedding into Aerosols and onto Fomites versus N2 Ab Fold Increase with acute infection serum as pre-existing Ab marker when possible-1.pdf}
\caption{Figure 2 (zoomed in). Fold-increase in N2 antibodies by H3
shedding into aerosols and onto fomites during infection}
\end{figure}

\hypertarget{correlation-plots-pre-existing-serum-samples-for-ab-assessment-taken-from-acute-infection-as-opposed-to-at-baseline-where-available}{%
\subsubsection{Correlation plots (pre-existing serum samples for Ab
assessment taken from acute infection, as opposed to at baseline, where
available)}\label{correlation-plots-pre-existing-serum-samples-for-ab-assessment-taken-from-acute-infection-as-opposed-to-at-baseline-where-available}}

\begin{figure}
\centering
\includegraphics{report_main_text_files/figure-latex/Correlation between H3 flu shedding (MT, aerosol, fomite) and pre-existing H3 immuno assays-1.pdf}
\caption{Figure a. Spearman correlation coefficients between H3 viral
shedding (collected from MT swab, aerosol, fomite) and pre-existing H3
antibodies. P-values in red: \textless0.1 = period, \textless0.5 =
asterisk, \textless0.01 = double asterisk, \textless0.001 = triple
asterisk.}
\end{figure}

\begin{figure}
\centering
\includegraphics{report_main_text_files/figure-latex/Correlation between H3 flu shedding (MT, aerosol, fomite) and H3 immuno assays fold increase with acute infection pre-existing-1.pdf}
\caption{Figure b. Spearman correlation coefficients between H3 viral
shedding (collected from MT swab, aerosol, fomite) and fold-increase in
H3 antibodies. P-values in red: \textless0.1 = period, \textless0.5 =
asterisk, \textless0.01 = double asterisk, \textless0.001 = triple
asterisk.}
\end{figure}

\hypertarget{predicting-aerosol-shedders-by-immunologic-assay-increase-induction-threshold}{%
\subsubsection{Predicting aerosol shedders by immunologic assay increase
(induction)
threshold}\label{predicting-aerosol-shedders-by-immunologic-assay-increase-induction-threshold}}

\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-1.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-2.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-3.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-4.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-5.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-6.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-7.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-8.pdf}

\begin{verbatim}

Call:
glm(formula = aerosol_positive ~ `N2 ELISA AUC`, family = binomial, 
    data = n2_inc_infectionbase)

Deviance Residuals: 
    Min       1Q   Median       3Q      Max  
-1.5617  -0.5847  -0.3041  -0.2833   1.9733  

Coefficients:
               Estimate Std. Error z value Pr(>|z|)  
(Intercept)    -3.32949    1.41547  -2.352   0.0187 *
`N2 ELISA AUC`  0.07823    0.04492   1.742   0.0816 .
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

(Dispersion parameter for binomial family taken to be 1)

    Null deviance: 15.844  on 16  degrees of freedom
Residual deviance: 12.015  on 15  degrees of freedom
AIC: 16.015

Number of Fisher Scoring iterations: 5
\end{verbatim}

\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-9.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-10.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-11.pdf}

\begin{verbatim}

Call:
glm(formula = aerosol_positive ~ `N2 inhibition titer`, family = binomial, 
    data = n2_inc_infectionbase)

Deviance Residuals: 
    Min       1Q   Median       3Q      Max  
-1.5975  -0.5574  -0.4322  -0.3908   1.6938  

Coefficients:
                      Estimate Std. Error z value Pr(>|z|)  
(Intercept)            -2.8310     1.2310  -2.300   0.0215 *
`N2 inhibition titer`   0.2620     0.1895   1.383   0.1668  
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

(Dispersion parameter for binomial family taken to be 1)

    Null deviance: 15.844  on 16  degrees of freedom
Residual deviance: 13.621  on 15  degrees of freedom
AIC: 17.621

Number of Fisher Scoring iterations: 4
\end{verbatim}

\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-12.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-13.pdf}
\includegraphics{report_main_text_files/figure-latex/aero shedding predictive value by fold increase in N2 ELISA-14.pdf}

\hypertarget{symptom-data-sociodemographics-health-history}{%
\subsubsection{Symptom data, sociodemographics, health
history}\label{symptom-data-sociodemographics-health-history}}

\begin{longtable}[]{@{}lccr@{}}
\toprule
& 0 (N=15) & 1 (N=3) & p value\tabularnewline
\midrule
\endhead
age\_earliest & & & 0.631\tabularnewline
- Mean (SD) & 19.7 (1.3) & 19.3 (0.7) &\tabularnewline
- Range & 18.5 - 22.9 & 18.7 - 20.0 &\tabularnewline
sex\_earliest & & & 0.396\tabularnewline
- 0 & 9 (60.0\%) & 1 (33.3\%) &\tabularnewline
- 1 & 6 (40.0\%) & 2 (66.7\%) &\tabularnewline
bmi\_earliest & & & 0.804\tabularnewline
- Mean (SD) & 24.3 (4.1) & 23.7 (3.3) &\tabularnewline
- Range & 19.6 - 35.6 & 20.0 - 26.4 &\tabularnewline
asthma & & & 0.396\tabularnewline
- 0 & 13 (86.7\%) & 2 (66.7\%) &\tabularnewline
- 1 & 2 (13.3\%) & 1 (33.3\%) &\tabularnewline
allerg & & & 0.598\tabularnewline
- 0 & 11 (73.3\%) & 3 (100.0\%) &\tabularnewline
- 1 & 3 (20.0\%) & 0 (0.0\%) &\tabularnewline
- 8 & 1 (6.7\%) & 0 (0.0\%) &\tabularnewline
copd & & & 0.645\tabularnewline
- 0 & 14 (93.3\%) & 3 (100.0\%) &\tabularnewline
- 8 & 1 (6.7\%) & 0 (0.0\%) &\tabularnewline
cf & & & 0.005\tabularnewline
- 0 & 15 (100.0\%) & 3 (100.0\%) &\tabularnewline
bronchitis & & & 0.005\tabularnewline
- 0 & 15 (100.0\%) & 3 (100.0\%) &\tabularnewline
emphys & & & 0.005\tabularnewline
- 0 & 15 (100.0\%) & 3 (100.0\%) &\tabularnewline
\bottomrule
\end{longtable}

\begin{longtable}[]{@{}lccr@{}}
\toprule
& 0 (N=15) & 1 (N=3) & p value\tabularnewline
\midrule
\endhead
asymptomatic & & & 0.005\tabularnewline
- 0 & 15 (100.0\%) & 3 (100.0\%) &\tabularnewline
ever\_fever & & & 0.645\tabularnewline
- 0 & 14 (93.3\%) & 3 (100.0\%) &\tabularnewline
- 1 & 1 (6.7\%) & 0 (0.0\%) &\tabularnewline
ever\_cough\_sx & & & 0.502\tabularnewline
- 0 & 2 (13.3\%) & 0 (0.0\%) &\tabularnewline
- 1 & 13 (86.7\%) & 3 (100.0\%) &\tabularnewline
ever\_cough\_sx2 & & & 0.310\tabularnewline
- 0 & 4 (26.7\%) & 0 (0.0\%) &\tabularnewline
- 1 & 11 (73.3\%) & 3 (100.0\%) &\tabularnewline
ever\_srthr\_sx & & & 0.396\tabularnewline
- 0 & 3 (20.0\%) & 0 (0.0\%) &\tabularnewline
- 1 & 12 (80.0\%) & 3 (100.0\%) &\tabularnewline
ever\_srthr\_sx2 & & & 0.090\tabularnewline
- 0 & 8 (53.3\%) & 0 (0.0\%) &\tabularnewline
- 1 & 7 (46.7\%) & 3 (100.0\%) &\tabularnewline
flu\_shot\_cur\_prior\_to\_obs & & & 0.007\tabularnewline
- N-Miss & 5 & 2 &\tabularnewline
- 1 & 10 (100.0\%) & 1 (100.0\%) &\tabularnewline
flu\_shot\_cur\_and\_prev & & & 0.919\tabularnewline
- N-Miss & 2 & 1 &\tabularnewline
- 0 & 6 (46.2\%) & 1 (50.0\%) &\tabularnewline
- 1 & 7 (53.8\%) & 1 (50.0\%) &\tabularnewline
oral\_temp & & & 0.681\tabularnewline
- Mean (SD) & 37.2 (0.3) & 37.1 (0.2) &\tabularnewline
- Range & 36.8 - 37.5 & 36.9 - 37.3 &\tabularnewline
up\_resp\_score & & & 0.116\tabularnewline
- Mean (SD) & 5.0 (2.7) & 8.0 (3.6) &\tabularnewline
- Range & 0.0 - 10.0 & 4.0 - 11.0 &\tabularnewline
lower\_resp\_score & & & 0.899\tabularnewline
- Mean (SD) & 3.1 (2.6) & 3.3 (1.5) &\tabularnewline
- Range & 0.0 - 9.0 & 2.0 - 5.0 &\tabularnewline
syst\_sx\_score & & & 0.062\tabularnewline
- Mean (SD) & 3.1 (2.7) & 6.3 (0.6) &\tabularnewline
- Range & 0.0 - 8.0 & 6.0 - 7.0 &\tabularnewline
total\_sx\_score & & & 0.156\tabularnewline
- Mean (SD) & 11.9 (7.3) & 18.3 (2.9) &\tabularnewline
- Range & 0.0 - 25.0 & 15.0 - 20.0 &\tabularnewline
cough & & & 0.129\tabularnewline
- Mean (SD) & 1.7 (1.0) & 2.7 (0.6) &\tabularnewline
- Range & 0.0 - 3.0 & 2.0 - 3.0 &\tabularnewline
cough\_count & & & 0.915\tabularnewline
- N-Miss & 1 & 0 &\tabularnewline
- Mean (SD) & 11.8 (17.0) & 10.7 (9.6) &\tabularnewline
- Range & 0.0 - 66.0 & 2.0 - 21.0 &\tabularnewline
Upper respiratory swab Ct & & & 0.117\tabularnewline
- Mean (SD) & 28.2 (5.0) & 23.3 (2.3) &\tabularnewline
- Range & 20.6 - 40.0 & 21.2 - 25.8 &\tabularnewline
antivir\_taken\_wi\_24h & & & 0.020\tabularnewline
- N-Miss & 3 & 0 &\tabularnewline
- 0 & 12 (100.0\%) & 3 (100.0\%) &\tabularnewline
\bottomrule
\end{longtable}

\hypertarget{si-figures}{%
\subsubsection{SI figures}\label{si-figures}}

\begin{figure}
\centering
\includegraphics{report_main_text_files/figure-latex/H3 Infection Boxplots Pre-existing and Fold-Increase Immunology Data with Infection Baseline-1.pdf}
\caption{Figure 1. Population baseline antibody levels against H3
(N=18), N2 (N=18), H1 (N=13), and N1 (N=13) and fold-increase in
antibody levels against H3 (N=17), N2 (N=17), H1 (N=12), N1 (N=12)
following infection among those with H3 infection}
\end{figure}

\begin{figure}
\centering
\includegraphics{report_main_text_files/figure-latex/H3 Infection Detectable Viral Shedding in Aerosol and Fomite versus Pre-existing and fold-increase in Ab with Baseline Preexisting Ab when available-1.pdf}
\caption{Figure SI. Pre-existing and fold-increase in antibody levels
(from baseline when possible) by H3 shedding into aerosols and onto
fomites during acute infection}
\end{figure}

\begin{figure}
\centering
\includegraphics{report_main_text_files/figure-latex/SI figure. Correlation between H3 flu shedding (MT, aerosol, fomite) and pre-existing Ab from baseline sample H3 immuno assays-1.pdf}
\caption{SI corr plot Figure a. Spearman correlation coefficients
between H3 viral shedding (collected from MT swab, aerosol, fomite) and
pre-existing Ab from baseline serum H3 antibody assays. P-values in red:
\textless0.1 = period, \textless0.5 = asterisk, \textless0.01 = double
asterisk, \textless0.001 = triple asterisk.}
\end{figure}

\begin{figure}
\centering
\includegraphics{report_main_text_files/figure-latex/SI figure. Correlation between H3 flu shedding (MT, aerosol, fomite) and H3 immuno assays fold increase with earliest pre-existing serum sample-1.pdf}
\caption{SI corr plot Figure b. Spearman correlation coefficients
between H3 viral shedding (collected from MT swab, aerosol, fomite) and
fold-increase in H3 antibodies, with earliest pre-existing serum sample
for Ab assessment. P-values in red: \textless0.1 = period, \textless0.5
= asterisk, \textless0.01 = double asterisk, \textless0.001 = triple
asterisk.}
\end{figure}

\hypertarget{discussion}{%
\subsection{Discussion}\label{discussion}}

\hypertarget{conclusion}{%
\subsection{Conclusion}\label{conclusion}}

\hypertarget{acknowledgements}{%
\subsection{Acknowledgements}\label{acknowledgements}}

This work was part of Prometheus-UMD, sponsored by the Defense Advanced
Research Projects Agency (DARPA) BTO under the auspices of Col. Matthew
Hepburn through agreements N66001-17-2-4023 and N66001-18-2-4015. The
findings and conclusions in this report are those of the authors and do
not necessarily represent the official position or policy of the funding
agency and no official endorsement should be inferred.

This work was also supported by the National Institute of Allergy and
Infectious Diseases Centers of Excellence for Influenza Research and
Surveillance (CEIRS) Grant Number HHSN272201400008C, and this funder had
no role in study design, data collection and analysis, decision to
publish, or preparation of the manuscript.

\hypertarget{analysis-notes}{%
\subsection{Analysis notes}\label{analysis-notes}}

\begin{verbatim}
R version 4.0.2 (2020-06-22)
Platform: x86_64-apple-darwin17.0 (64-bit)
Running under: macOS Catalina 10.15.7

Matrix products: default
BLAS:   /Library/Frameworks/R.framework/Versions/4.0/Resources/lib/libRblas.dylib
LAPACK: /Library/Frameworks/R.framework/Versions/4.0/Resources/lib/libRlapack.dylib

locale:
[1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8

attached base packages:
[1] tools     stats     graphics  grDevices utils     datasets  methods  
[8] base     

other attached packages:
 [1] arsenal_3.5.0              kableExtra_1.3.1          
 [3] pROC_1.16.2                viridis_0.5.1             
 [5] viridisLite_0.3.0          forcats_0.5.0             
 [7] stringr_1.4.0              dplyr_1.0.2               
 [9] purrr_0.3.4                readr_1.3.1               
[11] tidyr_1.1.2                tibble_3.0.3              
[13] tidyverse_1.3.0            maptools_1.0-2            
[15] rgdal_1.5-16               sp_1.4-2                  
[17] ggmap_3.0.0                reshape2_1.4.4            
[19] plyr_1.8.6                 PerformanceAnalytics_2.0.4
[21] xts_0.12.1                 zoo_1.8-8                 
[23] openxlsx_4.2.2             ggpubr_0.4.0              
[25] ggplot2_3.3.2.9000         scales_1.1.1              
[27] lubridate_1.7.9            boxr_0.3.5                
[29] rlang_0.4.7                knitr_1.30                

loaded via a namespace (and not attached):
 [1] bitops_1.0-6        fs_1.5.0            bit64_4.0.5        
 [4] webshot_0.5.2       httr_1.4.2          backports_1.1.10   
 [7] utf8_1.1.4          R6_2.4.1            DBI_1.1.0          
[10] colorspace_1.4-1    withr_2.3.0         tidyselect_1.1.0   
[13] gridExtra_2.3       bit_4.0.4           curl_4.3           
[16] compiler_4.0.2      cli_2.0.2           rvest_0.3.6        
[19] xml2_1.3.2          labeling_0.3        quadprog_1.5-8     
[22] askpass_1.1         digest_0.6.25       foreign_0.8-80     
[25] rmarkdown_2.3       rio_0.5.16          jpeg_0.1-8.1       
[28] pkgconfig_2.0.3     htmltools_0.5.0     highr_0.8          
[31] dbplyr_1.4.4        readxl_1.3.1        rstudioapi_0.11    
[34] farver_2.0.3        generics_0.0.2      jsonlite_1.7.1     
[37] zip_2.1.1           car_3.0-9           magrittr_1.5       
[40] Matrix_1.2-18       Rcpp_1.0.5          munsell_0.5.0      
[43] fansi_0.4.1         abind_1.4-5         lifecycle_0.2.0    
[46] stringi_1.5.3       yaml_2.2.1          carData_3.0-4      
[49] grid_4.0.2          blob_1.2.1          crayon_1.3.4       
[52] lattice_0.20-41     splines_4.0.2       haven_2.3.1        
[55] hms_0.5.3           pillar_1.4.6        rjson_0.2.20       
[58] ggsignif_0.6.0      reprex_0.3.0        glue_1.4.2         
[61] evaluate_0.14       data.table_1.13.0   modelr_0.1.8       
[64] png_0.1-7           vctrs_0.3.4         RgoogleMaps_1.4.5.3
[67] cellranger_1.1.0    gtable_0.3.0        openssl_1.4.3      
[70] assertthat_0.2.1    xfun_0.17           broom_0.7.0        
[73] rstatix_0.6.0       survival_3.1-12     ellipsis_0.3.1     
\end{verbatim}

\end{document}
