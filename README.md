# IMMUNITY_AND_SHEDDING

Mount Sinai has done immunology analysis of influenza A samples from Prometheus-UMD. We test the hypothesis that baseline immune markers predict influenza A viral shedding during acute illness.


1) _Find_flu_positive_samples_for_immune_analysis.R_ searches UMD database for samples that meet various criteria established for sample selection to be sent to Florian's group for lab evaluation.
2) _draft_sampl_selection.R_ supports the selection of samples to send to Florian's group for lab evaluation
3) _PROM_immuno_shedding_get_data_clean.R_ cleans data for analysis.
4) _report_main_text.Rmd_ contains the final script for analysis and initial draft of report document - produces report files as .docx, .pdf, and .html.
5) _report_draft.Rmd_ contains draft material used to support the report_main_text.Rmd file. 
6) _immuno_shedding_analysis.R_ contains script to reproduce analysis (archived -- important pieced moved to .Rmd analysis files)
7) _draft.R_ is a draft file of analysis (archived -- not needed to perform analysis)








